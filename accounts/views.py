from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import login


# Create your views here.

# Function View
    # If request is a form
        # Create form
        # If the form is valid, save user
        # Login the user w/ login() function
        # Redirect to "home"
    # Otherwise request is not POST
        # Go to user creation instead


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            # Optional message which doesn't work (yet) lol
            messages.success(request, "Account created successfully")
            return redirect("home")
    else:
        form = UserCreationForm()
    context = {"form": form, }
    # Render handles where to redirect and what data to pass to the template
    return render(request, "registration/signup.html", context)
